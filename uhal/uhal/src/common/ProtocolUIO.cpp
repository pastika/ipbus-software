/*
  ---------------------------------------------------------------------------

  This is an extension of uHAL to directly access AXI slaves via the linux
  UIO driver. 

  This file is part of uHAL.

  uHAL is a hardware access library and programming framework
  originally developed for upgrades of the Level-1 trigger of the CMS
  experiment at CERN.

  uHAL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  uHAL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with uHAL.  If not, see <http://www.gnu.org/licenses/>.


  Andrew Rose, Imperial College, London
  email: awr01 <AT> imperial.ac.uk

  Marc Magrans de Abril, CERN
  email: marc.magrans.de.abril <AT> cern.ch

  Tom Williams, Rutherford Appleton Laboratory, Oxfordshire
  email: tom.williams <AT> cern.ch

  Dan Gastler, Boston University 
  email: dgastler <AT> bu.edu
      
  Arnaud Steen, National Taiwan University 
  email: asteen <AT> cern.ch
  ---------------------------------------------------------------------------
*/
/**
   @file
   @author Siqi Yuan / Dan Gastler / Theron Jasper Tarigo / Arnaud Steen
*/

#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <algorithm>
#include <iomanip>
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/thread.hpp>
#include <uhal/Node.hpp>
#include <uhal/NodeTreeBuilder.hpp>
#include <pugixml.hpp>
#include "uhal/log/LogLevels.hpp"
#include "uhal/log/log_inserters.integer.hpp"
#include "uhal/log/log.hpp"

#include <uhal/ProtocolUIO.hpp>

#include <setjmp.h> //for BUS_ERROR signal handling

using namespace uioaxi;
using namespace boost::filesystem;

//#define DEBUG_AND_DUMP 1

//Signal handling for sigbus
sigjmp_buf static env;
void static signal_handler(int sig){
  if(SIGBUS == sig){
    siglongjmp(env,sig);    
  }
}
#define BUS_ERROR_PROTECTION(ACCESS)					\
  if(SIGBUS == sigsetjmp(env,1)){					\
    uhal::exception::UIOBusError * e = new uhal::exception::UIOBusError(); \
    throw *e;								\
  }else{								\
    ACCESS;								\
  }

namespace uhal {  

  UIO::UIO (
	    const std::string& aId, const URI& aUri,
	    const boost::posix_time::time_duration&aTimeoutPeriod
	    ) :
    ClientInterface(aId,aUri,aTimeoutPeriod)
  { 
    // Get the filename of the address table from the connection file. Then read it through the NodeTreeBuilder
    // The NodeTreeBuilder should be able to just use the existing node tree rather than rebuild a new one
    boost::shared_ptr< Node> lNode( NodeTreeBuilder::getInstance().getNodeTree ( std::string("file://")+aUri.mHostname , boost::filesystem::current_path() / "." ) );
    // Getting the IDs for only first layer nodes (nodes that contain device labels). matching names that doesn't contain a "."
    std::vector< std::string > top_node_Ids = lNode->getNodes("^[^.]+$");
    // For each device label, search for its matching device
    int size=0;
    
    for (auto nodeId :top_node_Ids) {
      log ( Notice() , "list of nodes ",nodeId.c_str(), " " );
    }    
    for (std::vector<std::string>::iterator nodeId = top_node_Ids.begin(); nodeId != top_node_Ids.end(); ++nodeId) {
      // device number is the number read from the most significant 8 bits of the address
      // size should be read from /sys/class/uio*/maps/map0/size
      char uioname[128]="", sizechar[128]="", addrchar[128]="";
      uint32_t address=0;
      std::string uiopath = "/sys/class/uio/";
      FILE *addrfile=0;
      FILE *sizefile=0; 
      bool hasInterrupt=false;
      // Traverse through the /sys/class/uio directory
      for (directory_iterator x(uiopath); x!=directory_iterator(); ++x){
	if (!is_directory(x->path())) {
	  continue;
	}
	if (!exists(x->path()/"maps/map0/addr")) {
	  continue;
	}
	if (!exists(x->path()/"maps/map0/size")) {
	  continue;
	}
	if (exists(x->path()/"device/of_node/interrupts"))
	    hasInterrupt=true;

	addrfile = fopen((x->path()/"maps/map0/addr").native().c_str(),"r");
	fgets(addrchar,128,addrfile); fclose(addrfile);
	address = std::strtoul( addrchar, 0, 16);
	if (lNode->getNode(*nodeId).getAddress() == address){
	  sizefile = fopen((x->path().native()+"/maps/map0/size").c_str(),"r");
	  fgets(sizechar,128,sizefile); fclose(sizefile);
	  //the size is in number of bytes, will be converted in 32-bit words after mmap
	  size=std::strtoul( sizechar, 0, 16);  
	  strcpy(uioname,x->path().filename().native().c_str());
	  break;
	}
      }
      if (size==0) {
	uhal::exception::BadUIODevice1* lExc = new uhal::exception::BadUIODevice1();
	log (*lExc , "Error: Trouble loading device ",(*nodeId).c_str(), " cannot find device or size zero.");
	throw *lExc;
	return;
      }
      openDevice(address, size, uioname,hasInterrupt);
    }
    
    //let's check all nodes to see if they corrsepond to an interrupt register:
    for( auto node_name : lNode->getNodes() ){
      uint32_t address = lNode->getNode(node_name).getAddress();
      std::string tags = lNode->getNode(node_name).getTags();
      if( tags.find("interrupt") == std::string::npos )
	continue;
      auto baseAddr = findBaseAddress( address );
      m_uiomap[ baseAddr ]->m_interruptAddressVec.push_back( address );
    }
  
    //Now that everything created sucessfully, we can deal with signal handling
    memset(&saBusError,0,sizeof(saBusError)); //Clear struct
    saBusError.sa_handler = signal_handler; //assign signal handler
    sigemptyset(&saBusError.sa_mask);
    sigaction(SIGBUS, &saBusError,&saBusError_old);  //install new signal handler (save the old one)
  }

  UIO::~UIO () {
    log ( Debug() , "UIO: destructor" );
    sigaction(SIGBUS,&saBusError_old,NULL); //restore the signal handler from before creation for SIGBUS
  }

  void 
  UIO::openDevice(uint32_t addr, uint32_t length, const char *name, bool hasInterrupt) {
    log ( Debug() , "UIO: openDevice : addr = ", Integer(addr,IntFmt<hex,fixed>()), " ; length = ", Integer(length), " ; name of device = /dev/", name);
    if( m_uiomap.find(addr)!=m_uiomap.end() ){
      uhal::exception::BadUIODevice0* lExc = new uhal::exception::BadUIODevice0();
      log (*lExc , "Address ", Integer(addr,IntFmt<hex,fixed>()) , " already used by a UIODevice -> check the address table file");
      throw *lExc;
      return;
    }

    std::ostringstream os(std::ostringstream::ate);
    os.str("");
    os << "/dev/" << name;
    uioaxi::UIOMemory_handler* uiomh = new uioaxi::UIOMemory_handler(os.str().c_str(),length,hasInterrupt);
    m_uiomap.insert( std::pair<uint32_t,uioaxi::UIOMemory_handler*>(addr,uiomh) );
    m_base_addrs.push_back( addr );
    log ( Debug(), "Mapped ", os.str().c_str(), " as device with a size of  ", Integer(length,IntFmt<hex,fixed>()), "; registered with address ", Integer(addr,IntFmt<hex,fixed>()) );


#ifdef DEBUG_AND_DUMP
    uint32_t iaddr32(0);
    os.str("");
    while( iaddr32 < uiomh->m_region.get_size()/4 ){
      os <<  "0x" << std::hex << std::setw(8) << std::setfill('0') << uiomh->m_data[iaddr32] << "  ";
      iaddr32++;
      if(iaddr32%16==0)
	os << "\n";
    }
    os << std::endl;
    log ( Debug(), "Dumping memory of device " , uiomh->m_name , ":\n", os.str().c_str() );
#endif
  }

  int
  UIO::checkDevice (uint32_t addr) {
    if( m_uiomap.find(addr)!=m_uiomap.end() ){
      uhal::exception::BadUIODevice1* lExc = new uhal::exception::BadUIODevice1();
      log (*lExc , "No device with address ", Integer(addr, IntFmt< hex, fixed>() ));
      throw *lExc;
      return 1;
    }
    return 0;
  }

  uint32_t 
  UIO::findBaseAddress(const uint32_t& aAddr) {
    uint32_t aMin(0xffffffff);
    uint32_t baseAddr(0);
    for(auto addr: m_base_addrs){
      uint32_t count = aAddr ^ addr;
      if( count<aMin ){
	baseAddr = addr;
	aMin = count;
      }
    }//OK c'est pas beau
    log ( Debug(), "Register address " , Integer(aAddr, IntFmt< hex, fixed>() ) , ", \t returned base address : ", Integer(baseAddr, IntFmt< hex, fixed>() ) );
    return baseAddr;
  }

  ValHeader
  UIO::implementWrite (const uint32_t& aAddr, const uint32_t& aValue) {
    //let's check value when the node comes with a bit mask
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t writeval = aValue;
    m_uiomap[baseAddr]->m_data[aAddr-baseAddr] = writeval;
    return ValHeader();
  }

  ValHeader 
  UIO::implementBOT(){
    log ( Debug() , "Byte Order Transaction");
    uhal::exception::UnimplementedFunction* lExc = new uhal::exception::UnimplementedFunction();
    log (*lExc, "Function implementBOT() is not yet implemented.");
    throw *lExc;
    return ValHeader();
  }

  ValHeader 
  UIO::implementWriteBlock (const uint32_t& aAddr, const std::vector<uint32_t>& aValues, const defs::BlockReadWriteMode& aMode) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t lAddr ( aAddr-baseAddr );
    auto aHandler = m_uiomap[baseAddr];
    // let's try this ugly code:
    std::vector<uint32_t>::const_iterator ptr;
    for (ptr = aValues.begin(); ptr < aValues.end(); ptr++){
      uint32_t writeval = *ptr;
      aHandler->m_data[lAddr] = writeval;
      if ( aMode == defs::INCREMENTAL )
	lAddr ++;
    }
    // // or we can try :
    // if ( aMode == defs::INCREMENTAL )
    //   std::memcpy( &aHandler.m_data[lAddr], &aValues[0], aValues.size() );
    // else
    //   std::memcpy( &aHandler.m_data[lAddr], &aValues[0], 1 );
    return ValHeader();
  }

  ValWord<uint32_t>
  UIO::implementRead (const uint32_t& aAddr, const uint32_t& aMask) {
    uint32_t baseAddr = findBaseAddress(aAddr);

    auto uiohandler = m_uiomap[baseAddr];
    auto iter = std::find( uiohandler->m_interruptAddressVec.begin(),
			   uiohandler->m_interruptAddressVec.end(),
			   aAddr
			   );
    if( uiohandler->m_hasInterrupt && iter!=uiohandler->m_interruptAddressVec.end() ){
      // reading this reg will trigger a blocking read until the next interrupt
      // then the register is cleared by writing "aMask" which will set the register back to 0
      // so in this case, the returned value of this function must be 0
      uint32_t unmask = 1;
      ssize_t rv = pwrite(uiohandler->m_fdfile, &unmask, sizeof(unmask), 0);
      if (rv != (ssize_t)sizeof(unmask)) {
	perror("UIO::unmask_interrupt()");
      }
      int count;
      int retval=pread(uiohandler->m_fdfile,&count,sizeof(count), 0);
      if (retval == -1)
	perror("UIO::read_interrrupt()");
    }

    //      m_uiomap[baseAddr]->m_data[aAddr-baseAddr]=aMask;
    uint32_t readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    ValWord<uint32_t> vw(readval, aMask);
    valwords.push_back(vw);
    
    primeDispatch();
    return vw;
  }
    
  ValVector< uint32_t > 
  UIO::implementReadBlock ( const uint32_t& aAddr, const uint32_t& aSize, const defs::BlockReadWriteMode& aMode ) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t lAddr = aAddr-baseAddr;    
    auto uiohandler = m_uiomap[baseAddr];
    

    std::vector<uint32_t> read_vector;read_vector.reserve(aSize);
    if( lAddr+aSize > uiohandler->m_region.get_size() ){
      uhal::exception::UIOAddressSpaceError* lExc = new uhal::exception::UIOAddressSpaceError();
      log (*lExc , "UIO device " , uiohandler->m_name , " can not read/write into a memory block of size " , Integer(aSize) , " from address ", Integer(aAddr,IntFmt<hex,fixed>()) );
      throw *lExc;
      return ValVector< uint32_t>(read_vector); //this is not a validated memory anyway
    }
    else
      std::copy( &uiohandler->m_data[lAddr],uiohandler->m_data+lAddr+aSize,std::back_inserter(read_vector) );
    
    primeDispatch();

    ValVector< uint32_t>  vv(read_vector);
    vv.valid(true);
    return vv;
  }

  void
  UIO::primeDispatch () {
    // uhal will never call implementDispatch unless told that buffers are in
    // use (even though the buffers are not actually used and are length zero).
    // implementDispatch will only be called once after each checkBufferSpace.
    uint32_t sendcount = 0, replycount = 0, sendavail, replyavail;
    checkBufferSpace ( sendcount, replycount, sendavail, replyavail);
  }

  void
  UIO::implementDispatch (boost::shared_ptr<Buffers> aBuffers) {
    log ( Debug(), "UIO: Dispatch");
    for (unsigned int i=0; i<valwords.size(); i++)
      valwords[i].valid(true);
    valwords.clear();
  }

  ValWord<uint32_t> UIO::implementRMWbits ( const uint32_t& aAddr , const uint32_t& aANDterm , const uint32_t& aORterm ){
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    readval &= aANDterm;
    readval |= aORterm;
    m_uiomap[baseAddr]->m_data[aAddr-baseAddr] = readval;
    readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    return ValWord<uint32_t>(readval);
  }


  ValWord< uint32_t > UIO::implementRMWsum ( const uint32_t& aAddr , const int32_t& aAddend ) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    readval += aAddend;
    m_uiomap[baseAddr]->m_data[aAddr-baseAddr] = readval;
    readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    return ValWord<uint32_t>(readval);
  }
}
